import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { CoffesModule } from './coffes/coffes.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { CoffeesBiz } from './x-state/coffees.biz';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxsModule.forFeature([]),
    BrowserAnimationsModule,
    MaterialModule,
    CoffesModule,
    HttpClientModule,
    NgxsModule.forRoot(),
  ],
  providers: [CoffeesBiz],
  bootstrap: [AppComponent]
})
export class AppModule { }
