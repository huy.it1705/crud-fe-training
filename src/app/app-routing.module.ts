import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    { path: 'coffees', loadChildren: () => import('./coffes/coffes.module').then(m => m.CoffesModule)},
    { path: 'edit', loadChildren: () => import('./edit-coffees/edit-coffees.module').then(m => m.EditCoffeesModule) },
    { path: 'edit/:id', loadChildren: () => import('./edit-coffees/edit-coffees.module').then(m => m.EditCoffeesModule) }];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
