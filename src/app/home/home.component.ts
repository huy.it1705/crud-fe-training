import { Component, OnInit } from '@angular/core';
import { CoffeesService } from '../coffes/coffees-services/coffees.service';
import { Observable } from 'rxjs';
import { Coffees } from '../coffes/coffees-model/coffees.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  coffees: Observable<Coffees>;
  constructor(private coffeesService: CoffeesService) { }

  ngOnInit(): void {
  }

}
