import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Coffees } from '../coffees-model/coffees.model';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CoffeesService {
  constructor(private http: HttpClient) { }

  getCoffees(): Observable<Coffees[]> {
    return this.http.get<Coffees[]>(environment.url);
  }
  getCoffeeById(id): Observable<Coffees> {
    return this.http.get<Coffees>(`${environment.url}/${id}`);
  }
  createCoffee(coffees: Coffees): Observable<Coffees> {
    return this.http.post<Coffees>(environment.url, coffees);
  }
  editCoffee(id, coffees: Coffees): Observable<Coffees> {
    return this.http.put<Coffees>(`${environment.url}/${id}`, coffees);
  }
  removeCoffee(id): Observable<Coffees> {
    return this.http.delete<Coffees>(`${environment.url}/${id}`);
  }
}
