import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoffeesComponent } from './coffees.component';
import { CoffesRoutingModule } from './coffees-routing.module';
import { MaterialModule } from '../material.module';
import { ConfirmRemoveComponent } from './confirm-remove/confirm-remove.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { CoffeesBiz } from '../x-state/coffees.biz';



@NgModule({
  declarations: [CoffeesComponent, ConfirmRemoveComponent],
  imports: [
    CommonModule,
    CoffesRoutingModule,
    MaterialModule,
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  entryComponents: [ConfirmRemoveComponent]
})
export class CoffesModule { }
