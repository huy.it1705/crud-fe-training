import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { CoffeesService } from '../coffees-services/coffees.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Coffees } from '../coffees-model/coffees.model';
import { DeleteConfirmed, DeleteRequested, DeleteCanceled } from 'src/app/x-state/state-machine-event';
import { CoffeesBiz } from 'src/app/x-state/coffees.biz';

@Component({
  selector: 'app-confirm-remove',
  templateUrl: './confirm-remove.component.html',
  styleUrls: ['./confirm-remove.component.css']
})
export class ConfirmRemoveComponent implements OnInit {
  idCoffee: string;
  coffees: Coffees[];
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<ConfirmRemoveComponent>,
              private coffeesService: CoffeesService,
              private snackBar: MatSnackBar,
              private coffeesBiz: CoffeesBiz) { }

  ngOnInit(): void {
    this.idCoffee = this.data;
  }
  onDelete() {
    if (this.idCoffee) {
      this.coffeesService.removeCoffee(this.idCoffee).subscribe((res) => {
        if (res) {
          this.coffeesBiz.transition(new DeleteConfirmed(this.idCoffee));
          this.snackBar.open('Xóa thành công', '', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }, (err) => {
        if (err) {
          this.coffeesBiz.transition(new DeleteCanceled());
          this.snackBar.open('Xóa thất bại', '', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      });
    }
    this.onClose();
  }
  onClose() {
    this.dialogRef.close();
  }
}
