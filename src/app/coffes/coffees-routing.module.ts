import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CoffeesComponent } from './coffees.component';

const routes: Routes = [
    {
        path: '',
        component: CoffeesComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoffesRoutingModule {}
