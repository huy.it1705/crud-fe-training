import { Component, OnInit } from '@angular/core';
import { CoffeesService } from './coffees-services/coffees.service';
import { Coffees } from './coffees-model/coffees.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmRemoveComponent } from './confirm-remove/confirm-remove.component';
import { CoffeesBiz } from '../x-state/coffees.biz';
import { DeleteRequested } from '../x-state/state-machine-event';

@Component({
  selector: 'app-coffees',
  templateUrl: './coffees.component.html',
  styleUrls: ['./coffees.component.css']
})
export class CoffeesComponent implements OnInit {
  coffees: Observable<Coffees[]>;
  id: string;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private coffeesService: CoffeesService,
              private router: Router,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private coffeesBiz: CoffeesBiz) { }

  ngOnInit(): void {
    this.coffeesBiz.initStateMachine();
    this.getData();
  }
  getData() {
    this.coffeesService.getCoffees().subscribe((x: any) => {
      this.coffees = x;
    });
  }
  onRemove(id) {
    this.coffeesBiz.transition(new DeleteRequested());
    const dialogRef = this.dialog.open(ConfirmRemoveComponent, {
      data: id
    });
    dialogRef.afterClosed().subscribe((res) => {
      this.getData();
    });
  }
}
