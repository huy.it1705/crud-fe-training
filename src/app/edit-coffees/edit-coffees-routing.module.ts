import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCoffeesComponent } from './edit-coffees.component';

const routes: Routes = [{ path: '', component: EditCoffeesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditCoffeesRoutingModule { }
