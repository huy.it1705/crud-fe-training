import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CoffeesService } from '../coffes/coffees-services/coffees.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Coffees } from '../coffes/coffees-model/coffees.model';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { CoffeesBiz } from '../x-state/coffees.biz';
import {
  UpdateRequested, CreateRequested,
  UpdateConfirmed, CreateConfirmed,
  UpdateCanceled, CreateCanceled
} from '../x-state/state-machine-event';

@Component({
  selector: 'app-edit-coffees',
  templateUrl: './edit-coffees.component.html',
  styleUrls: ['./edit-coffees.component.css']
})
export class EditCoffeesComponent implements OnInit {
  frmEditCoffee: FormGroup;
  idCoffee: string;
  coffees: Coffees[] = [];
  state: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private fb: FormBuilder,
              private coffeeService: CoffeesService,
              private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              private coffeesBiz: CoffeesBiz) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((x: any) => {
      this.idCoffee = x.params.id;
    });
    if (this.idCoffee) {
      this.coffeeService.getCoffeeById(this.idCoffee).subscribe((x: any) => {
        this.coffeesBiz.transition(new UpdateRequested());
        this.coffees = x;
        this.frmEditCoffee = this.initForm(this.coffees);
      });
    } else {
      this.coffeesBiz.transition(new CreateRequested());
    }
    this.frmEditCoffee = this.initForm(this.coffees);
  }
  private initForm(formData): FormGroup {
    const frm = this.fb.group({
      title: [formData && formData.title, Validators.required],
      author: [formData && formData.author, Validators.required],
    });
    return frm;
  }
  onSave() {
    this.frmEditCoffee.markAllAsTouched();
    if (this.frmEditCoffee.invalid) {
      return;
    }
    const data = this.frmEditCoffee.getRawValue();
    if (this.idCoffee) {
      this.coffeeService.editCoffee(this.idCoffee, data).subscribe((res) => {
        if (res) {
          this.coffeesBiz.transition(new UpdateConfirmed(this.idCoffee, data));
          this.snackBar.open('Sửa thành công', '', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.router.navigateByUrl('/coffees');
        }
      }, (err) => {
        this.coffeesBiz.transition(new UpdateCanceled());
        this.snackBar.open('Sửa thất bại', '', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
    } else {
      this.coffeeService.createCoffee(data).subscribe((res) => {
        if (res) {
          this.coffeesBiz.transition(new CreateConfirmed(data));
          this.snackBar.open('Thêm thành công', '', {
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.router.navigateByUrl('/coffees');
        }
      }, (err) => {
        this.coffeesBiz.transition(new CreateCanceled());
        this.snackBar.open('Thêm thất bại', '', {
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
    }
  }
}
