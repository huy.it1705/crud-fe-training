import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditCoffeesRoutingModule } from './edit-coffees-routing.module';
import { EditCoffeesComponent } from './edit-coffees.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoffeesBiz } from '../x-state/coffees.biz';


@NgModule({
  declarations: [EditCoffeesComponent],
  imports: [
    CommonModule,
    EditCoffeesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: []
})
export class EditCoffeesModule { }
