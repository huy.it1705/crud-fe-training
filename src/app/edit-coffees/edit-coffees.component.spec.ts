import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCoffeesComponent } from './edit-coffees.component';

describe('EditCoffeesComponent', () => {
  let component: EditCoffeesComponent;
  let fixture: ComponentFixture<EditCoffeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCoffeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCoffeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
