import { MachineConfig } from 'xstate';
import {
    CoffeesEvents, FetchSucceed, FetchFailed, CreateRequested,
    UpdateRequested, DeleteRequested, CreateConfirmed, CreateCanceled,
    CreateSucceed, CreateFailed, UpdateConfirmed, UpdateCanceled, UpdateSucceed,
    UpdateFailed, DeleteCanceled, DeleteSucceed, DeleteFailed
} from './state-machine-event';

export interface CoffeesSchema {
    states: {
        idle: {};
        fetching: {};
        showing: {};
        create: {};
        create_processing: {};
        update: {};
        update_processing: {};
        delete: {};
        delete_processing: {};
        errors: {};
    };
}

export const CoffeesConfig: MachineConfig<
    any,
    CoffeesSchema,
    CoffeesEvents> = {
    id: 'coffees_state_machine',
    initial: 'idle',
    states: {
        idle: {
            on: {
                '': 'fetching',
            },
        },
        fetching: {
            invoke: {
                id: 'fetchCoffees',
                src: 'fetchCoffees',
            },
            on: {
                [FetchSucceed.type]: {
                    target: 'showing',
                    actions: ['showMessages'],
                },
                [FetchFailed.type]: 'errors',
            },
        },
        showing: {
            on: {
                [CreateRequested.type]: 'create',
                [UpdateRequested.type]: 'update',
                [DeleteRequested.type]: 'delete',
            },
        },
        create: {
            on: {
                [CreateConfirmed.type]: 'create_processing',
                [CreateCanceled.type]: 'showing',
            },
        },
        create_processing: {
            invoke: {
                id: 'createCoffees',
                src: 'createCoffees',
            },
            on: {
                [CreateSucceed.type]: {
                    target: 'showing',
                    actions: ['showMessages'],
                },
                [CreateFailed.type]: {
                    target: 'showing',
                    actions: 'showMessages',
                },
            },
        },
        update: {
            on: {
                [UpdateConfirmed.type]: 'update_processing',
                [UpdateCanceled.type]: 'showing',
            },
        },
        update_processing: {
            invoke: {
                id: 'updateCoffees',
                src: 'updateCoffees',
            },
            on: {
                [UpdateSucceed.type]: {
                    target: 'showing',
                    actions: ['showMessages'],
                },
                [UpdateFailed.type]: {
                    target: 'showing',
                    actions: 'showMessages',
                },
            },
        },
        delete: {
            on: {
                [CreateConfirmed.type]: 'delete_processing',
                [DeleteCanceled.type]: 'showing',
            },
        },
        delete_processing: {
            invoke: {
                id: 'deleteCoffees',
                src: 'deleteCoffees',
            },
            on: {
                [DeleteSucceed.type]: {
                    target: 'showing',
                    actions: ['showMessages'],
                },
                [DeleteFailed.type]: {
                    target: 'showing',
                    actions: 'showMessages',
                },
            },
        },
        errors: {
            type: 'final',
        },
    },
};
