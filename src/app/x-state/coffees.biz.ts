import { Injectable } from '@angular/core';
import { CoffeesService } from '../coffes/coffees-services/coffees.service';
import { MachineOptions, AnyEventObject, Machine, interpret, EventObject } from 'xstate';
import {
    CoffeesEvents, FetchSucceed, FetchFailed,
    CreateConfirmed, CreateSucceed, CreateFailed,
    UpdateConfirmed, UpdateSucceed, UpdateFailed,
    DeleteConfirmed, DeleteSucceed, DeleteFailed
} from './state-machine-event';
import { map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { CoffeesSchema, CoffeesConfig } from './state-machine-config';
import { Coffees } from '../coffes/coffees-model/coffees.model';

@Injectable()
export class CoffeesBiz {
    constructor(private coffeesService: CoffeesService) { }

    private service;
    private machine;
    private coffeesOptions: Partial<
        MachineOptions<any, CoffeesEvents>
        > = {
            actions: {
                showMessages: (_, event: AnyEventObject) => {
                    event.messages.forEach((msg) => console.log(msg));
                },
            },
            services: {
                fetchCoffees: () => {
                    return this.coffeesService.getCoffees().pipe(
                        map((tasks) => {
                            return new FetchSucceed(tasks, ['Fetched succeed!']);
                        }),
                        catchError(() => {
                            return of(new FetchFailed(['Fetched failed!']));
                        })
                    );
                },
                createCoffees: (_, event: CreateConfirmed) => {
                    return this.coffeesService.createCoffee(event.createCoffees).pipe(
                        map((createdCoffees) => {
                            return new CreateSucceed(['Created succeed!']);
                        }),
                        catchError(() => {
                            return of(new CreateFailed(['Create failed!']));
                        })
                    );
                },
                updateCoffees: (_, event: UpdateConfirmed) => {
                    return this.coffeesService.editCoffee(event.id, event.update).pipe(
                        map((updatedCoffees) => {
                            return new UpdateSucceed(['Update succeed!']);
                        }),
                        catchError(() => {
                            return of(new UpdateFailed(['Update failed!']));
                        })
                    );
                },
                deleteCoffees: (_, event: DeleteConfirmed) => {
                    return this.coffeesService.removeCoffee(event.id).pipe(
                        map(() => {
                            return new DeleteSucceed(['Delete succeed!']);
                        }),
                        catchError(() => {
                            return of(new DeleteFailed(['Delete failed!']));
                        })
                    );
                },
            },
        };

    initStateMachine(): void {
        this.machine = Machine<any, CoffeesSchema, CoffeesEvents>(
            CoffeesConfig
        ).withConfig(this.coffeesOptions);

        this.service = interpret(this.machine)
            .onTransition((transition) =>
                console.log(`Current state: ${transition.value}`)
            )
            .start();
    }

    transition(event: EventObject): void {
        this.service.send(event);
    }

    getData(): Observable<Coffees[]> {
        return this.coffeesService.getCoffees();
    }

    createCoffee(createTasks: Coffees): Observable<Coffees> {
        return this.coffeesService.createCoffee(createTasks);
    }

    updateCoffee(id: string, updateCoffees: Coffees): void {
        this.coffeesService.editCoffee(id, updateCoffees).subscribe();
    }

    deleteCoffee(id: string): void {
        this.coffeesService.removeCoffee(id).subscribe();
    }
}
