import { EventObject } from 'xstate';
import { Coffees } from '../coffes/coffees-model/coffees.model';

export class FetchSucceed implements EventObject {
    static readonly type = 'Fetch Succeed';
    readonly type = FetchSucceed.type;

    constructor(public coffees: Coffees[], public messages: string[]) { }
}
export class FetchFailed implements EventObject {
    static readonly type = 'Fetch Failed';
    readonly type = FetchFailed.type;
    constructor(public messages: string[]) { }
}
export class CreateRequested implements EventObject {
    static readonly type = 'Create Requested';
    readonly type = CreateRequested.type;
    constructor() { }
}
export class CreateConfirmed implements EventObject {
    static readonly type = 'Create Confirmed';
    readonly type = CreateConfirmed.type;
    constructor(public createCoffees: Coffees) { }
}
export class CreateCanceled implements EventObject {
    static readonly type = 'Create Canceled';
    readonly type = CreateCanceled.type;
    constructor() { }
}
export class CreateSucceed implements EventObject {
    static readonly type = 'Create Succeed';
    readonly type = CreateSucceed.type;
    constructor(public messages: string[]) { }
}
export class CreateFailed implements EventObject {
    static readonly type = 'Create Failed';
    readonly type = CreateFailed.type;
    constructor(public messages: string[]) { }
}
export class UpdateRequested implements EventObject {
    static readonly type = 'Update Requested';
    readonly type = UpdateRequested.type;
    constructor() { }
}
export class UpdateConfirmed implements EventObject {
    static readonly type = 'Update Confirmed';
    readonly type = UpdateConfirmed.type;
    constructor(public id: string, public update: Coffees) { }
}
export class UpdateCanceled implements EventObject {
    static readonly type = 'Update Canceled';
    readonly type = UpdateCanceled.type;
    constructor() { }
}
export class UpdateSucceed implements EventObject {
    static readonly type = 'Update Succeed';
    readonly type = UpdateSucceed.type;

    constructor(public messages: string[]) { }
}
export class UpdateFailed implements EventObject {
    static readonly type = 'Update Failed';
    readonly type = UpdateFailed.type;

    constructor(public messages: string[]) { }
}

export class DeleteRequested implements EventObject {
    static readonly type = 'Delete Requested';
    readonly type = DeleteRequested.type;

    constructor() { }
}

export class DeleteConfirmed implements EventObject {
    static readonly type = 'Delete Confirmed';
    readonly type = DeleteConfirmed.type;

    constructor(public id: string) { }
}

export class DeleteCanceled implements EventObject {
    static readonly type = 'Delete Canceled';
    readonly type = DeleteCanceled.type;

    constructor() { }
}

export class DeleteSucceed implements EventObject {
    static readonly type = 'Delete Succeed';
    readonly type = DeleteSucceed.type;

    constructor(public messages: string[]) { }
}

export class DeleteFailed implements EventObject {
    static readonly type = 'Delete Failed';
    readonly type = DeleteFailed.type;

    constructor(public messages: string[]) { }
}
export type CoffeesEvents =
  | FetchSucceed     | FetchFailed      | CreateRequested  | CreateConfirmed
  | CreateCanceled   | CreateSucceed    | CreateFailed     | UpdateRequested
  | UpdateConfirmed  | UpdateCanceled   | UpdateSucceed    | UpdateFailed
  | DeleteRequested  | DeleteConfirmed  | DeleteCanceled   | DeleteSucceed   | DeleteFailed;
