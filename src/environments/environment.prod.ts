export const environment = {
  production: true,
  url: 'https://server-coffees-crud.herokuapp.com/coffees',
};
